
# Prépare-toi pour l’examen

> **Dès le début du projet que tu comptes présenter à l’examen du titre, tu peux commencer à réfléchir et à rédiger les différents documents qui devront être prêts le jour de l’examen.**

## 1. Dossier projet 

> **Le dossier projet est l’équivalent d’un rapport de stage. Il doit comporter environ 48 750 signes espaces non compris (soit 30 à 35 pages hors annexes) et suivre le plan-type suivant:**

* Liste des compétences du référentiel qui sont couvertes par le projet
* Résumé du projet en français d’une longueur d’environ 20 lignes soit 200 à 250 mots, ou environ 1200 caractères espaces non compris
* Réalisations du candidat comportant les extraits de code les plus significatifs et en les argumentant, y compris pour la sécurité et le web mobile
* Présentation du jeu d’essai élaboré par le candidat de la fonctionnalité la plus représentative (données en entrée, données attendues, données obtenues)
* Description de la veille, effectuée par le candidat durant le projet, sur les vulnérabilités de sécurité
* Description d’une situation de travail ayant nécessité une recherche, effectuée par le candidat durant le projet, à partir de site anglophone
* Extrait du site anglophone, utilisé dans le cadre de la recherche décrite précédemment, accompagné de la traduction en français effectuée par le candidat sans traducteur automatique (environ 750 signes).

### 2. Support de présentation du projet

> **Pour la présentation orale du projet sur lequel tu as travaillé, tu dois préparer un support sur lequel t’appuyer. Il doit suivre le plan-type suivant :**

* Présentation de l'entreprise et/ou du service
* Contexte du projet (cahier des charges, environnement humain et technique)
* Conception et codage des composants front-end et des composants back-end
* Présentation ou démonstration des éléments les plus significatifs de l'interface de l'application
* Présentation du jeu d’essai de la fonctionnalité la plus représentative (données en entrée, données attendues, données obtenues) et analyse des écarts éventuels
* Présentation d’un exemple de recherche effectuée à partir de site anglophone
* Synthèse et conclusion (satisfactions et difficultés rencontrées)

> __Cette présentation inclut, au choix, une illustration ou une démonstration de l’interface de l’application. Une combinaison des deux est possible. Leur durée cumulée dans la présentation ne doit pas excéder 10 minutes__

#### 3. Dossier professionnel (DP)

> **Dans le dossier professionnel, tu vas décrire par activité type et à partir d’exemples concrets les pratiques professionnelles en rapport direct avec le titre professionnel “développeur web et web mobile”. Un modèle vierge de DP te sera envoyé par ton campus manager ainsi qu’un guide pour le remplir**
